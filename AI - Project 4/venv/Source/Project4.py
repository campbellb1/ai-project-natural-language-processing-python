"""
Project 4:      Naturally Language Processing
Programmer: Brandon Campbell
Date:       4/8/2018
Purpose:
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.cluster import AgglomerativeClustering
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
import os
from gensim import corpora, models, similarities
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from nltk.stem import PorterStemmer
import operator
import matplotlib.pyplot as plt
from nltk import ngrams
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from nltk.stem import WordNetLemmatizer
import gensim


speechList = {}
tokenList = {}
for speechFile in os.listdir("../speeches"):
    file = open("../speeches/" + speechFile)
    text = file.read()
    speechList[speechFile] = text;

# loop through document list


for speechName, speechText in speechList.items():
    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(speechText)
    tokens = [t.lower() for t in tokens]
    tokens = tokens[:]
    sr = stopwords.words('english')
    tokens = [i for i in tokens if not i in sr]

    lemmatizer = WordNetLemmatizer()
    lemmatized_tokens = [lemmatizer.lemmatize(token) for token in tokens]

    speechList[speechName] = "";
    tokenList[speechName] = lemmatized_tokens
    speechList[speechName] = ' '.join(lemmatized_tokens)

    # pos = nltk.pos_tag(lemmatized_tokens)
    # pos_counts = {}
    # for key, val in pos:
    #     print(str(key) + ':' + str(val))
    #     if val not in pos_counts.keys():
    #         pos_counts[val] = 1
    #     else:
    #         pos_counts[val] += 1
    # print(pos_counts)
    # plt.bar(range(len(pos_counts)), list(pos_counts.values()), align='center')
    # plt.xticks(range(len(pos_counts)), list(pos_counts.keys()))
    #plt.show()

    # print("Frequency Analysis...")
    # freq = nltk.FreqDist(lemmatized_tokens)
    # for key, val in freq.items():
    #     print(str(key) + ':' + str(val))
    # print("Length of Unique Items:", len(freq.items()))
    # freq.plot(10, cumulative=False)
    myGrams = ngrams(speechList[speechName].split(), 3)
    for gram in myGrams: print(gram)


docs = speechList.values()
vec = CountVectorizer()
X = vec.fit_transform(docs)
df = pd.DataFrame(X.toarray(), columns=vec.get_feature_names())
df.to_csv("PresedentialSpeechMatrix.csv", ",")


# turn our tokenized documents into a id <-> term dictionary
dictionary = corpora.Dictionary(tokenList.values())

# convert tokenized documents into a document-term matrix
corpus = [dictionary.doc2bow(text) for text in tokenList.values()]

# generate LDA model
ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=50, id2word=dictionary, passes=60)
print(ldamodel.print_topics(25, 3))
newList = []
for key, value in speechList.items():
    newList = newList + value.split()

print("Frequency Analysis...")
freq = nltk.FreqDist(newList)
for key, val in freq.items():
    print(str(key) + ':' + str(val))
print("Length of Unique Items:", len(freq.items()))
freq.plot(40, cumulative=False)

x = []
y = []

for n_clust in range(2, 51):
    kmeans = KMeans(init='k-means++', n_clusters=n_clust)
    fit = kmeans.fit(df.values)
    x.append(n_clust)
    y.append(fit.inertia_)
# plt.plot(x,y)
# plt.title("Graph")
# plt.xlabel("Clusters")
# plt.ylabel("Inertia")
# plt.show()
# print("KMEANS: k = {0} inertia = {1}".format(n_clust, fit.inertia_))



